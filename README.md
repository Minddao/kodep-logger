### What is this repository for? ###
 
Сборка для логирования ruby on reails приложений, основанная на docker-compose elasticsearch, kibana и logstash

Elasticsearch - это поисковая система, основанная на Lucene. Она предоставляет собой распределенную, полнотекстовую поисковую систему с веб-интерфейсом HTTP и документами JSON. Elasticsearch разработан на Java и выпущен в виде открытого исходного кода в соответствии с условиями лицензии Apache.

Kibana - это инструмент для визуализации данных Elasticsearch

Logstash - это инструмент для сбора и парсинга логов.
 
### How do I get set up? ### 
 
Настройки elasticsearch находятся по пути elasticsearch/config/elasticsearch.yml

Настройки kibana находятся по пути kibana/config/kibana.yml

Настройки logstash находятся по пути logstash/config/logstash.yml

Настройки логирования logstash находятся по пути logstash/pipeline/logstash.conf

Основным файлом для работы с перенаправлением логов является logstash.conf
 
### Contribution guidelines ###
 
Elasticsearch работает на 2 портах 9300 и 9200, слушая 9200

Kibana работает на 5601, принимая данные от elasticsearch

Logstash слушает 5000